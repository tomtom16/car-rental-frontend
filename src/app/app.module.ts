import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import {LoginService} from './services/login.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule, MatDividerModule, MatFormFieldModule,
  MatInputModule, MatToolbarModule, MatSelectModule, MatCheckboxModule,
  MatDatepickerModule, MatNativeDateModule, MatTableModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import { RentalComponent } from './components/rental/rental.component';
import {AuthenticationGuard} from './services/authentication.guard';
import {HttpClientModule} from '@angular/common/http';
import {OrderService} from './services/order.service';
import { MapComponent } from './components/map/map.component';
import { AgmCoreModule } from '@agm/core';
import { AdminComponent } from './components/admin/admin.component';


@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    RentalComponent,
    MapComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,

    // Angular Material imports
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatCardModule,
    MatDividerModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDQGakIAyVrDDVFEDrnTBZvbNBTcSLWJXk'
      }),

  ],
  providers: [
    LoginService,
    AuthenticationGuard,
    OrderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
