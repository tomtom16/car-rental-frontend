import {User} from './user.model';
import {Category} from './category.model';

export class Order {
  id: number;
  status: string;
  fromDate: Date;
  toDate: Date;
  location: Location;
  user: User;
  category: Category;
  includesInsurance: boolean;
  price: number;
  currency: string;
}
