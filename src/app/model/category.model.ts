export class Category {
  name: string;
  price: number;
  insurancePrice: number;
  currency: string;
}
