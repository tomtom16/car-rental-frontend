export class User {
  username: string;
  password: string
  passwordSecond: string;
  firstName: string;
  lastName: string;
  email: string;
}
