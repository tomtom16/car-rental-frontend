export class Location {
  id: number;
  city: string;
  country: string;
  latitude: number;
  longitude: number;
}
