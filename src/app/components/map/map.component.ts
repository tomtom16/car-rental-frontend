import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AgmMap} from '@agm/core';
import {Location} from '../../model/location.model';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  latitude = 48.209;
  longitude = 16.37;
  mapType = 'roadmap';

  @ViewChild('AgmMap') agmMap: AgmMap;

  @Input()
  locations: Location[];

  @Output()
  onLocationSelected: EventEmitter<Location> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }


  markerClicked(marker: Location) {
    this.onLocationSelected.emit(marker);
  }
}
