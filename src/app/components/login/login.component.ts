import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  failure: boolean;

  constructor(private service: LoginService, private builder: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.form = this.builder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
      }
    );
  }


  onSubmit() {
    this.failure = false;
    this.service.login({
      username: this.form.get('username').value,
      password: this.form.get('password').value,
    }).subscribe(value => {
      if (value) {
        this.router.navigateByUrl('/rental');
      } else {
        this.failure = true;
      }
    });
  }

}
