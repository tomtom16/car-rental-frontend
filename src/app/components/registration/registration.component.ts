import {Component, OnInit} from '@angular/core';
import {FormBuilder,FormControl, FormGroup, FormGroupDirective,NgForm,   Validators} from '@angular/forms';
import {LoginService} from '../../services/login.service';
import {Observable} from 'rxjs';
import {ErrorStateMatcher} from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.hasError('notSame') && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;

  registrationStatus$: Observable<boolean>;

  matcher = new MyErrorStateMatcher();
  requestSent: boolean;


  constructor(private service: LoginService, private builder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.builder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
        passwordSecond: [''],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        mail: ['', Validators.compose([Validators.required, Validators.email])],
      } ,{ validator: this.checkPasswords }
    );
  }


  onSubmit() {
    this.registrationStatus$ = this.service.registerUser({
      username: this.form.get('username').value,
      password: this.form.get('password').value,
      passwordSecond: this.form.get('passwordSecond').value,
      firstName: this.form.get('firstName').value,
      lastName: this.form.get('lastName').value,
      email: this.form.get('mail').value
    });
    this.registrationStatus$.subscribe(value => this.requestSent = true);
    this.form.reset();
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.passwordSecond.value;

    return pass === confirmPass ? null : { notSame: true }
  }
}
