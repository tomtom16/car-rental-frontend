import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {OrderService} from '../../services/order.service';
import {Observable, of} from 'rxjs';
import {Location} from '../../model/location.model';
import {LoginService} from '../../services/login.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

form: FormGroup;
form2: FormGroup;

failure: boolean;

lat: number = 48.17;
lng: number = 16.37;
latStart: number = 48.17;
lngStart: number = 16.37;

locationCountryControl = new FormControl('', [Validators.required]);
locationCityControl = new FormControl('', [Validators.required]);
carNameControl = new FormControl('', [Validators.required]);

locationControl = new FormControl('', [Validators.required]);
locations$: Observable<Location[]> = of([]);

constructor(private builder: FormBuilder, private router: Router, private service: OrderService, private userService: LoginService) {
}

ngOnInit(): void {
  this.form = this.builder.group({
      locationCity: ['', Validators.required],
      locationCountry: ['', Validators.required],
      //latitude: ['', Validators.required],
      //longitude: ['', Validators.required]
    }
  );
  this.form2 = this.builder.group({
    location: ['', Validators.required],
    carName: ['', Validators.required]
  }
  );
  
  this.loadLocations();

}

onChoseLocation(event){
  console.log(event);
  this.lat=event.coords.lat;
  this.lng=event.coords.lng;
}

onSubmit() {
  /*this.service.xxx(this.getNewLocation()).subscribe(value => {

    window.scroll(0, 0);
  });*/
  this.form.reset();
  this.lat=this.latStart;
  this.lng=this.lngStart;
}

onSubmit_Car() {
  /*this.service.xxx(this.getNewCar()).subscribe(value => {

    window.scroll(0, 0);
  });*/
  this.form2.reset();
}

private getNewLocation() {
  return {
    locationCity: this.form.get('locationCity').value,
    locationCountry: this.form.get('locationCountry').value,
    latitude: this.form.get('latitude').value,
    longitude: this.form.get('longitude').value,
  };
}

private getNewCar() {
  return {
    carName: this.form2.get('carName').value,
    location: this.form2.get('location').value,
  };
}

private loadLocations() {
  this.locations$ = this.service.loadLocations();
}

locationSelected(location: Location) {
  this.form.get('location').setValue(location, {onlySelf: true});
}

compareFn(c1: Location, c2: Location): boolean {
  return c1 && c2 && c1.id === c2.id;
}

logout() {
  this.userService.logout().subscribe(value => this.router.navigateByUrl('/login'));
}
}
