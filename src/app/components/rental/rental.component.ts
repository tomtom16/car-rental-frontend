import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {OrderService} from '../../services/order.service';
import {Category} from '../../model/category.model';
import {Observable, of} from 'rxjs';
import {Order} from '../../model/order.model';
import {Location} from '../../model/location.model';
import {LoginService} from '../../services/login.service';

export interface Currency {
  name: string;
  value: string;
}


@Component({
  selector: 'app-rental',
  templateUrl: './rental.component.html',
  styleUrls: ['./rental.component.scss']
})

export class RentalComponent implements OnInit {

  displayedColumns: string[] = ['category', 'fromDate', 'toDate', 'location', 'price'];

  form: FormGroup;
  failure: boolean;

  locationControl = new FormControl('', [Validators.required]);
  categoryControl = new FormControl('', [Validators.required]);
  currencyControl = new FormControl('', [Validators.required]);

  locations$: Observable<Location[]> = of([]);
  categories: Observable<Category[]> = of([]);
  currencies: Currency[] = [
    {name: 'Euro', value: 'EUR'},
    {name: 'Dollar', value: 'USD'},
  ];
  calculatedOrder: Observable<Order>;
  orders: Observable<Order[]>;

  constructor(private builder: FormBuilder, private router: Router, private service: OrderService, private userService: LoginService) {
  }

  ngOnInit(): void {
    this.form = this.builder.group({
        location: ['', Validators.required],
        category: ['', Validators.required],
        currency: ['', Validators.required],
        fromDate: ['', Validators.required],
        toDate: ['', Validators.required],
        includesInsurance: [false]
      }
    );

    this.form.get('category').disable();
    this.form.get('currency').valueChanges.subscribe(value => this.currencySelected(value));
    this.loadUserOrders();
    this.loadLocations();
  }

  currencySelected(currency: Currency) {
    if (currency) {
      this.form.get('category').disable();
      this.categories = this.service.getCategories(currency.value);
      this.categories.subscribe(value => this.form.get('category').enable());
    }
  }

  onSubmit() {
    this.service.doOrder(this.getOrder()).subscribe(value => {
      this.loadUserOrders();
      window.scroll(0, 0);
    });
    this.form.reset();
  }

  calculatePrice() {
    this.calculatedOrder = this.service.calculatePrice(this.getOrder());
  }

  private getOrder() {
    return {
      category: {'id': this.form.get('category').value.id},
      location: {'id': this.form.get('location').value.id},
      includesInsurance: this.form.get('includesInsurance').value,
      currency: this.form.get('currency').value.value,
      fromDate: this.form.get('fromDate').value,
      toDate: this.form.get('toDate').value
    };
  }

  private loadUserOrders() {
    this.orders = this.service.getOrders();
  }

  private loadLocations() {
    this.locations$ = this.service.loadLocations();
  }

  locationSelected(location: Location) {
    this.form.get('location').setValue(location, {onlySelf: true});
  }

  compareFn(c1: Location, c2: Location): boolean {
    return c1 && c2 && c1.id === c2.id;
  }

  getCurrency(currency: string) {
    return this.currencies.find(c => c.value === currency).name;
  }

  logout() {
    this.userService.logout().subscribe(value => this.router.navigateByUrl('/login'));
  }
}
