import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegistrationComponent} from './components/registration/registration.component';
import {LoginService} from './services/login.service';
import {LoginComponent} from './components/login/login.component';
import {AuthenticationGuard} from './services/authentication.guard';
import {RentalComponent} from './components/rental/rental.component';
import {AdminComponent} from './components/admin/admin.component';
import {MapComponent} from './components/map/map.component';

const routes: Routes = [
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'admin',
    component: AdminComponent,
  },
  {
    path: 'rental',
    component: RentalComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: '**',
    redirectTo: '/rental'
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
