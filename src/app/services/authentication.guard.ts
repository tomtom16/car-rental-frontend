import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {LoginService} from './login.service';
import {tap} from 'rxjs/operators';

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(private service: LoginService, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // check if authenticated
    return this.service.isLoggedIn().pipe(tap(isLoggedIn => {
      if (!isLoggedIn) {
        this.router.navigateByUrl('/login');
      }
    }));
  }

}
