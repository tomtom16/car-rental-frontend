import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable, of} from 'rxjs';
import {Category} from '../model/category.model';
import {catchError, map, share} from 'rxjs/operators';
import {Order} from '../model/order.model';
import {Location} from '../model/location.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) {
  }

  getCategories(currency: any): Observable<Category[]> {
    return this.http.get<Category[]>(environment.apiUrl + '/categories/' + currency).pipe(
      share()
    );
  }

  calculatePrice(order): Observable<Order> {
    return this.http.post(environment.apiUrl + '/calculate', order).pipe(
      map((calculatedOrder: any) => calculatedOrder),
      share()
    );
  }

  doOrder(order): Observable<boolean> {
    return this.http.post(environment.apiUrl + '/orders', order, {observe: 'response'}).pipe(
      map(res => res.status === 200),
      share(),
      catchError(_ => of(false))
    );
  }

  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(environment.apiUrl + '/orders').pipe(
      share()
    );
  }

  loadLocations(): Observable<Location[]> {
    return this.http.get<Location[]>(environment.apiUrl + '/locations').pipe(
      share()
    );
  }
}
