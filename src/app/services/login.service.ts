import {Injectable} from '@angular/core';
import {User} from '../model/user.model';
import {Observable, of} from 'rxjs';
import {LoginRequest} from '../model/login-request.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, map, share} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {

  }


  registerUser(user: User): Observable<boolean> {
    return this.http.post(environment.apiUrl + '/register', user)
      .pipe(
        map(response => response.hasOwnProperty('id')),
        share()
      );
  }

  login(loginRequest: LoginRequest): Observable<boolean> {
    // save access token?
    const formData = new FormData();
    formData.append('username', loginRequest.username);
    formData.append('password', loginRequest.password);

    return this.http.post(environment.apiUrl + '/login', formData, {observe: 'response'})
      .pipe(
        map(res => res.status === 200),
        share(),
        catchError(error => of(false))
      );
  }

  isLoggedIn(): Observable<boolean> {
    return this.http.get(environment.apiUrl + '/user', {observe: 'response'})
      .pipe(
        map(res => res.status === 200),
        share(),
        catchError(error => of(false))
      );
  }

  logout(): Observable<any> {
    return this.http.get(environment.apiUrl + '/logout')
      .pipe(
        map(v => true),
        share(),
        catchError(e => of(true))
      );
  }
}
